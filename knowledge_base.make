includes[openoutreach] = "http://drupalcode.org/project/openoutreach.git/blob_plain/refs/tags/7.x-1.0-beta8:/openoutreach.make"

includes[debut] = "debut_overrides.make.inc"
includes[debut_event] = "debut_event_overrides.make.inc"

;CIT documentation

projects[cck_blocks][type] = "module"
projects[cck_blocks][subdir] = "custom"
projects[cck_blocks][version] = "1.0"

projects[ds][type] = "module"
projects[ds][subdir] = "custom"
projects[ds][version] = "1.4"

projects[entity][type] = "module"
projects[entity][subdir] = "custom"
projects[entity][version] = "1.0-beta10"

projects[feeds][type] = "module"
projects[feeds][subdir] = "custom"
projects[feeds][version] = "2.0-alpha4"

projects[feeds_tamper][type] = "module"
projects[feeds_tamper][subdir] = "custom"
projects[feeds_tamper][version] = "1.0-beta3"

projects[feeds_xpathparser][type] = "module"
projects[feeds_xpathparser][subdir] = "custom"
projects[feeds_xpathparser][version] = "1.0-beta2"

projects[job_scheduler][type] = "module"
projects[job_scheduler][subdir] = "custom"
projects[job_scheduler][version] = "2.0-alpha2"

projects[superfish][type] = "module"
projects[superfish][subdir] = "custom"
projects[superfish][version] = "1.8"

projects[shadowbox][type] = "module"
projects[shadowbox][subdir] = "custom"
projects[shadowbox][version] = "3.0-beta6"

projects[syntaxhighlighter][type] = "module"
projects[syntaxhighlighter][subdir] = "custom"
projects[syntaxhighlighter][version] = "1.1"

projects[views_data_export][type] = "module"
projects[views_data_export][subdir] = "custom"
projects[views_data_export][version] = "3.0-beta5"

; Libraries
libraries[superfish][download][type] = "get"
libraries[superfish][download][url] = "https://github.com/mehrpadin/Superfish-for-Drupal"
libraries[superfish][directory_name] = "superfish"

libraries[syntaxhighlighter][download][type] = "get"
libraries[syntaxhighlighter][download][url] = "https://github.com/alexgorbatchev/SyntaxHighlighter"
libraries[syntaxhighlighter][directory_name] = "syntaxhighlighter"


;custom

projects[stock_list][type] = "module"
projects[stock_list][download][type] = "git"
projects[stock_list][download][url] = "urbanbassman@git.drupal.org:sandbox/urbanbassman/1485926.git"
projects[stock_list][subdir] = "custom"

projects[locomotive][type] = "module"
projects[locomotive][download][type] = "git"
projects[locomotive][download][url] = "urbanbassman@git.drupal.org:sandbox/urbanbassman/1489942.git"
projects[locomotive][subdir] = "custom"

projects[locomotive_sample_content][type] = "module"
projects[locomotive_sample_content][download][type] = "git"
projects[locomotive_sample_content][download][url] = "urbanbassman@git.drupal.org:sandbox/urbanbassman/1493122.git"
projects[locomotive_sample_content][subdir] = "custom"

projects[article_sample_content][type] = "module"
projects[article_sample_content][download][type] = "git"
projects[article_sample_content][download][url] = "urbanbassman@git.drupal.org:sandbox/urbanbassman/1503180.git"
projects[article_sample_content][subdir] = "custom"

projects[heritage_rail_home_page][type] = "module"
projects[heritage_rail_home_page][download][type] = "git"
projects[heritage_rail_home_page][download][url] = "urbanbassman@git.drupal.org:sandbox/urbanbassman/1503228.git"
projects[heritage_rail_home_page][subdir] = "custom"

projects[backup_migrate][type] = "module"
projects[backup_migrate][subdir] = "custom"
projects[backup_migrate][version] = "2.2"

projects[features_extra][type] = "module"
projects[features_extra][subdir] = "custom"
projects[features_extra][version] = "1.x-dev"

projects[node_export][type] = "module"
projects[node_export][subdir] = "custom"
projects[node_export][version] = "3.x-dev"
projects[uuid][type] = "module"
;projects[uuid][subdir] = "custom"
;projects[uuid][version] = "1.0-alpha3"
projects[uuid][download][type] = "get"
projects[uuid][download][url] = "http://ftp.drupal.org/files/projects/uuid-7.x-1.x-dev.tar.gz"

;theme
projects[omega][type] = "module"
projects[omega][subdir] = "custom"

projects[zen][type] = "module"
projects[zen][subdir] = "custom"

projects[navin][type] = "module"
projects[navin][subdir] = "custom"

projects[lner][type] = "theme"
projects[lner][download][type] = "git"
projects[lner][download][url] = "urbanbassman@git.drupal.org:sandbox/urbanbassman/1493136.git"
projects[lner][subdir] = "custom"

projects[mind_games][type] = "theme"
projects[mind_games][download][type] = "git"
projects[mind_games][download][url] = "urbanbassman@git.drupal.org:sandbox/urbanbassman/1510666.git"
projects[mind_games][subdir] = "custom"

projects[cit][type] = "theme"
projects[cit][download][type] = "git"
projects[cit][download][url] = "urbanbassman@git.drupal.org:sandbox/urbanbassman/1509432.git"
projects[cit][subdir] = "custom"

;omega supporting modules
projects[omega_tools][type] = "module"
projects[omega_tools][subdir] = "custom"
projects[delta][type] = "module"
projects[delta][subdir] = "custom"

