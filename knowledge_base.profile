<?php

/**
 * @file
 * Installation profile for the Knowledge Base distribution.
 */

include_once('knowledge_base.features.inc');
// Include only when in install mode. MAINTENANCE_MODE is defined in
// install.php and in drush_core_site_install().
if (defined('MAINTENANCE_MODE') && MAINTENANCE_MODE == 'install') {
  include_once('knowledge_base.install.inc');
}

/**
 * Implements hook_modules_installed().
 *
 * When a module is installed, enable the modules it recommends if they are
 * present. For Knowledge Base, also install permissions.
 */
function knowledge_base_modules_installed($modules) {
  module_load_include('inc', 'knowledge_base', 'knowledge_base.module_batch');
  knowledge_base_module_batch($modules);
}

/**
 * Check that other install profiles are not present to ensure we don't collide with a
 * similar form alter in their profile.
 *
 * Set Knowledge Base as default install profile.
 */
if (!function_exists('system_form_install_select_profile_form_alter')) {
  function system_form_install_select_profile_form_alter(&$form, $form_state) {
    // Only set the value if Knowledge Base is the only profile.
    if (count($form['profile']) == 1) {
      foreach($form['profile'] as $key => $element) {
        $form['profile'][$key]['#value'] = 'knowledge_base';
      }
    }
  }
}
